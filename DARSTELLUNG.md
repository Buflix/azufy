# Aufgaben

## 1 Applikation starten
* Starte applikation mit docker-compose up -d
* installiere Abhängigkeiten mit composer
* Öffne http://127.0.0.1/

## 2 LuckyController
* Öffne auf der Startseite den Link **Create your first page**
* Erstelle den LuckyController und teste ihn

## 3 Annotation Routes
* stelle den LuckyController aut Annotation Routes um

## 4 FakeNewsController
* Erstelle einen neuen Controller "FakeNewsController"
* Gib unter der Route "/fakenews" die Fakenews aus
##### Format
`Beaking news: {info1} {info2} ...`
##### Daten
```php
$statements = [
    [
        'content' => 'Die Kuh gibt Milch',
        'fake' => false
    ],
    [
        'content' => 'Die Erde ist eine Scheibe.',
        'fake' => true
    ],
    [
        'content' => 'Composer installiert für mich PHP Libraries',
        'fake' => false
    ],
    [
        'content' => 'Das sind nicht die Droiden die ihr sucht.',
        'fake' => true
    ],
    [
        'content' => 'Niemand hat die Absicht, eine Mauer zu errichten!',
        'fake' => true
    ],
    [
        'content' => 'Nokia hat früher Gummistiefel hergestellt',
        'fake' => false
    ]
];
```

## 5 PowerOfTwoController
* Erstelle einen neuen Controller "PowerOfTwoController"
* Gib unter der Route "/poweroftwo/{exponent}" die Zweierpotenz des übergebenen Exponenten aus 
##### Format
`"Power of two with exponent 3 is: 8"`

## 6 Routen prüfen
Lass dir von der Symfony Console deine erzeugten Routen anzeigen

`docker-compose exec php ash`

`bin/console debug:router`

## 7 TenCommandmentsController
* Erstelle einen neuen Controller "TenCommandmentsController"
* Gib unter der Route "/commandment/{number}" als JsonResponse das Gebot zur angegeben Nummer zurück
* Ist die Nummer ungültig lautet die Antwort: "Schweig still Narr!"

##### Format
```json
{
  "answer": "Du sollst den Feiertag heiligen."
}
```
##### Daten
```php
$commandments = [
    'Ich bin der Herr, dein Gott. Du sollst keine anderen Götter haben neben mir.',
    'Du sollst den Namen des Herrn, deines Gottes, nicht mißbrauchen.',
    'Du sollst den Feiertag heiligen.',
    'Du sollst deinen Vater und deine Mutter ehren.',
    'Du sollst nicht töten.',
    'Du sollst nicht ehebrechen.',
    'Du sollst nicht stehlen.',
    'Du sollst nicht falsch Zeugnis reden wider deinen Nächsten.',
    'Du sollst nicht begehren deines Nächsten Haus.',
    'Du sollst nicht begehren deines Nächsten Weib, Knecht, Magd, Vieh noch alles, was dein Nächster hat.'
];
```

## 8 Pokemon Twig Templates
* Erstelle einen neuen Controller "PokemonController"
* Erstelle mit Hilfe der Daten unter "/pokemon" eine HTML Tabelle, die alle Daten anzeigt
* Nutze dazu Twig ([Doku](https://symfony.com/doc/current/page_creation.html#rendering-a-template))

##### Daten
```php
$pokemons = [
    [
        'number' => 49,
        'name' => 'Sandamer',
        'types' => [
            'Boden'
        ],
        'img' => 'https://www.pokewiki.de/images/2/2a/Pok%C3%A9mon-Icon_028.png'
    ],
    [
        'number' => 95,
        'name' => 'Onix',
        'types' => [
            'Gestein',
            'Boden'
        ],
        'img' => 'https://www.pokewiki.de/images/5/5a/Pok%C3%A9mon-Icon_095.png'
    ],
    [
        'number' => 98,
        'name' => 'Krabby',
        'types' => [
            'Wasser'
        ],
        'img' => 'https://www.pokewiki.de/images/0/00/Pok%C3%A9mon-Icon_098.png'
    ],
    [
        'number' => 148,
        'name' => 'Dragonir',
        'types' => [
            'Drache'
        ],
        'img' => 'https://www.pokewiki.de/images/3/30/Pok%C3%A9mon-Icon_148.png'
    ]
];
```

## 9 Pokemon Layout & Sub-Templates
* Benutze "base.html.twig" als Layout
* Erstelle ein Sub-Template für eine Tabellen-Zeile
