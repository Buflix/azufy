## Setup

### Clone Repo
```
git clone git@gitlab.com:Buflix/azufy.git
cd azufy
```

### Install Composer
```
./scripts/composer install
```

### Start Docker
```
docker-compose up
```

## Task
 - [Darstellung](DARSTELLUNG.md)
 - [Datenverarbeitung](DATENVERARBEITUNG.md)