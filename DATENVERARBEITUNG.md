# Aufgaben

## 1 PizzaOrder
* Erstelle einen neuen Controller "PizzaOrderController"
* Erstelle unter der Route "/pizza/order" ein Formular ([Doku](https://symfony.com/doc/current/forms.html))
* Validiere die gesendeten Daten und gib sie mit var_dump() aus
* Probiere weitere Form-Elemente aus

##### Formular
* type - Typ: Select[Margherita | Funghi]
* extra-cheese - Extra Käse: Checkbox
* user-id: Hidden[Fester Wert: 37]
* send - Abschicken: Submit

## 2 CharacterController CRUD
* Erstelle einen RedisService ([Docs](https://symfony.com/doc/current/service_container.html))
* Erstelle einen neuen Controller "CharacterController"
* Erstelle ein Formular und speichere angelegte Charaktere under dem Redis-Key 'char' (Route: "/character/add")
* Erstelle eine Route "/character/list" und stelle erstellte Charaktere Tabellarisch dar. Überlege dir eine Struktur um im Redis mehrere Charaktere anzulegen und auflisten zu können.
* Erstelle eine Route "/character/delete" und "/character/edit". Strukturiere deinen Code klug und vermeide Copy&Past Code
* Validiere, dass der Charakter-Name 2-12 Zeichen lang ist, keine Sonderzeichen enthält und speichere ihn mit großem Anfangsbuchstabe 

##### Redis Verbindung
```php
$redis = new \Redis();
$redis->connect('redis');
```

##### Formular
Elemente: name, race, class, gender, undead(true/false), cyborg(true/false), faction, birthday

##### Rassen
```php
[
    'Mensch' => 0,
    'Orc' => 1,
    'Zwerg' => 2,
    'Elf' => 3
]
```
##### Klassen
```php
[
    'Krieger' => 0,
    'Magier' => 1,
    'Priester' => 2,
    'Bogenschütze' => 3,
    'Schurke' => 4
]
```
##### Fraktionen
```php
[
    'Isengard' => 0,
    'Ravenclaw' => 1,
    'Die Blutsegelbukaniere' => 2,
    'Trevor Philips Industries' => 3,
    'Trinity' => 4
]
```

## 3 Pet JSON API CRUD
* Erstelle einen neuen Controller "PetController"
* Request & Response sollen JSON sein
* Erstelle "/pet/list|get|add|edit|delete/{ID}"
* Speichere im Redis